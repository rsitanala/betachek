jQuery = jQuery.noConflict();

jQuery(window).load(function() {
	var shippingUpdateFrm = jQuery('#co-shipping-method-form');
	var shippingInfo = jQuery('#shipmentInfo');
	var errorMessage = jQuery('.error-msg');
	var productOption = jQuery('#product-options-wrapper');
	var customerRevwr = jQuery('#customer-reviews');
	var productView = jQuery('.product-view');
	var error = jQuery('.validation-failed');
	var error2 = jQuery('.validation-advice');
	jQuery("#tabcontent").children('div').hide(); // Initially hide all content
	jQuery("#tabs li:first").attr("id","current"); // Activate first tab
	jQuery("#tabcontent div:first").fadeIn(); // Show first tab content
    
    jQuery('#tabs a').click(function(e) {
        e.preventDefault();        
        jQuery("#tabcontent").children('div').hide(); //Hide all content
        jQuery("#tabs li").attr("id",""); //Reset id's
        jQuery(this).parent().attr("id","current"); // Activate this
        jQuery('#' + jQuery(this).attr('title')).fadeIn(); // Show content for current tab
    });
	if( shippingUpdateFrm.length > 0 ) {
		jQuery('div.shipping').show();
	}
	if( shippingInfo.length > 0 ) {
		jQuery('#shippingLabel,#shippingPrice,div.shipping').hide();
	}
	if( errorMessage.length > 0 || productOption.length > 0 || customerRevwr.length > 0 ) {
		productView.show();
	}
	jQuery('#shippingLabel,#shippingPrice,#shipmentInfo,.shipping #closeButton').click(function() {
		jQuery('div.shipping').toggle();
	});
	jQuery('#customerLinks').click(function() {
		jQuery('#customerData').toggle();
	});
	jQuery("#storeTopList div").on('click', function(event){
		jQuery('.loadtext').show();
		jQuery('#storeTopList div').removeClass('active');
		jQuery(this).addClass('active');
		var link = jQuery(this).find('a').attr('href');
		jQuery('.product-view').css('opacity','0.5');
		jQuery('.product-view').load(link  +' .product-essential, #prodTabs', function() {
			setup_tab();
			gallery();
		});
		jQuery('.product-view').show(1000);
		jQuery('.product-view').css('opacity','1');
		jQuery('.loadtext').fadeOut(7500);
		event.preventDefault();
		return false;
	});
	function setup_tab () {
		jQuery("#tabcontent").children('div').hide(); // Initially hide all content
		jQuery("#tabs li:first").attr("id","current"); // Activate first tab
		jQuery("#tabcontent div:first").fadeIn(); // Show first tab content
		
		jQuery('#tabs a').click(function(e) {
			e.preventDefault();        
			jQuery("#tabcontent").children('div').hide(); //Hide all content
			jQuery("#tabs li").attr("id",""); //Reset id's
			jQuery(this).parent().attr("id","current"); // Activate this
			jQuery('#' + jQuery(this).attr('title')).fadeIn(); // Show content for current tab
		});
	}
	function gallery() {
        jQuery("a[rel=fancybox],a[rel=fancybox-main]").fancybox({
			'padding' : 10,
			'margin' : 40,
			'opacity' : 1,
			'scrolling' : 'auto',
			'autoScale' : 1,
			'hideOnOverlayClick' : 1,
			'overlayShow' : 1,
			'overlayOpacity' : 0.7,
			'overlayColor' : '#777',
			'titleShow' : 1,
			'transitionIn' : 'elastic',
			'transitionOut' : 'elastic',
			'speedIn' : 500,
			'speedOut' : 500,
			'showCloseButton' : 1,
			'showNavArrows' : 1,
			'enableEscapeButton' : 1
		});
	}
	jQuery('#g5ExtraData a, #popup, #gfxExtraData').click(function() {
		jQuery('#popup').toggle();
		jQuery('#middle object').toggle();
		return false;
	});
	
	jQuery('.guess_area button').click(function() {
		jQuery('#checkoutAddress, #step-1, #guestAddress').show('slow');
		jQuery('.guess_area button').hide('slow');
		return false;
	});
	
	jQuery('#step-1 .checkoutButtonRight').click(function() {
		var inputs = jQuery('#bill_form').find('.required-entry').filter(function() { return jQuery(this).val() == ""; });
		if (inputs.length > 3) {
			alert('please fill required input');
			return false;
		}
		else {
			jQuery('#shippingMethod, #step-2').show('slow');
			jQuery('#step-1').hide('slow');
			return false;
		}
	});
	
	jQuery('#step-2 .checkoutButtonRight').click(function() {
		var shipCheckbox = jQuery('.sp-methods').find('input[type="radio"]').filter(function() { return jQuery(this).attr('checked'); });
		if (shipCheckbox.length < 2) {
			alert('please select shipping method');
		}
		else {
			jQuery('#paymentMethod,#step-3').show('slow');
			jQuery('#step-2').hide('slow');
			return false;
		}
	});
	
	jQuery('#step-3 .checkoutButtonRight').click(function() {
		var inputs = jQuery('#paymentMethod').find('input[type="text"]').filter(function() { return jQuery(this).val() == ""; });
		var error = jQuery('#paymentMethod').find('input.validation-failed, select.validation-failed').filter(function() { return jQuery(this); });
		if (inputs.length > 2 || error.length > 0) {
			error.show();
			error2.show();
			jQuery('#reviewWrap').hide();
			jQuery('#step-3').show();
		}
		else {
			jQuery('#reviewWrap').show('slow');
			jQuery('#step-3').hide('slow');
			return false;
		}
	});
	
	function valid() {
		var inputs = jQuery('#paymentMethod').find('input[type="text"]').filter(function() { return jQuery(this).val() == ""; });
		var error = jQuery('#paymentMethod').find('input.validation-failed, select.validation-failed');
		return true;
		return inputs;
		return error;
	}
	
	jQuery(".content > p:contains('Powered by')").hide();
	
	jQuery('.buynow').attr('title',' ');
});