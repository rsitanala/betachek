jQuery(document).ready(function() {
		var rsi = jQuery('#slide').royalSlider({
		autoHeight: false,
		arrowsNav: false,
		fadeinLoadedSlide: true,
		controlNavigationSpacing: 0,
		controlNavigation: 'bullets',
		imageScaleMode: 'fill',
		imageAlignCenter: true,
		loop: true,
		loopRewind: true,
		numImagesToPreload: 6,
		keyboardNavEnabled: true,
		autoScaleSlider: true,  
		autoScaleSliderWidth: 980,     
		autoScaleSliderHeight: 502,

		/* size of all images http://help.dimsemenov.com/kb/royalslider-jquery-plugin-faq/adding-width-and-height-properties-to-images */
		imgWidth: 980,
		imgHeight: 502,
			autoPlay: {
				// autoplay options go gere
				enabled: true,
				pauseOnHover: true
			}



	  }).data('royalSlider');
	  jQuery('#slider-next').click(function() {
		rsi.next();
	  });
	  jQuery('#slider-prev').click(function() {
		rsi.prev();
	  });
	});